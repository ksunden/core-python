# The Zaber Core Serial Library in Python;

Thanks for choosing to use a Zaber device! This library is intended to help 
a Python software project communicate with Zaber devices quickly
and easily.

## Usage & Documentation

Full documentation and example code can be found online at
https://www.zaber.com/support/docs/api/core-python/.

## Installation

This library can be installed from PyPI using pip:
    
    pip install zaber.serial

It can also be installed directly using the ``setup.py`` file provided
with the source code::

    python setup.py install


## License & Source Code

This library is open-source, and is licensed under the Apache Software License 
Version 2.0. The source code is available at https://gitlab.com/zaber-core-libs/core-python.

The full text of the license can be seen in LICENSE.txt.


## Contact Us

If you need to contact Zaber for any reason, please send an email to
contact@zaber.com. More detailed contact information can be found online at
http://www.zaber.com/contact/. Please do not hesitate to ask us for help with
this library or our devices.

