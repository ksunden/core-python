import context
from zaber.serial import AsciiSerial
from zaber.serial.portlock import PortLock


class AsciiSerialWrapper(AsciiSerial):
    def __init__(self, aPort):
        self._ser = aPort
        self._ser.open()
        self._lock = PortLock()
