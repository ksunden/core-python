import context
from zaber.serial import BinarySerial
from zaber.serial.portlock import PortLock


class BinarySerialWrapper(BinarySerial):
    def __init__(self, aPort):
        self._ser = aPort
        self._ser.open()
        self._lock = PortLock()
