exceptions module
=================

.. autoclass:: zaber.serial.TimeoutError
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: zaber.serial.UnexpectedReplyError
    :members:
    :undoc-members:
    :show-inheritance:

