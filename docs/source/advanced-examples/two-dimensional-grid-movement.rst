Two-Dimensional Grid Movement
=============================

This example shows how to perform two-dimensional grid movement using two devices.
One device is for X axis and second for Y axis.
Goal is to visit predefined points within the grid.

The movement is performed on grid of size 40 x 20 mm. First point is on coordinates x=2 mm, y=2 mm.
Step on X axis is 5 mm and on axis is Y 4 mm respectively. See picture below.

.. image:: grid-movement.png
    :width: 500px
    :align: center
    :alt: Grid movement image

Program first homes both axis and the moves through all the points. At the end both axis are homed again.
Program stops for 1 second on each point and prints its coordinates. You can always stop program by pressing ``Ctrl+C``.

Before you begin with code you should use Zaber Console to connect to port and identify both devices.
Make sure to change the port within code to appropriate one:

.. literalinclude::  ../code/two_dimensional_grid_movement.py
    :lines: 4-5

It is important that devices are properly numbered (have correct numbers assigned).
Renumbering of devices can be performed using Zaber Console.
Examples assumes that X-axis device has number 1 and Y-axis has number 2.

.. literalinclude::  ../code/two_dimensional_grid_movement.py
    :lines: 7-9

Example is using mathematical conversion between device specific microsteps and millimeters.
Correct conversion results depend on right value of constants:

.. literalinclude::  ../code/two_dimensional_grid_movement.py
    :lines: 11-13

Please set appropriate values depending on what devices are used.
Values can be found in devices' datasheets as "Microstep Size (Default Resolution)".

Example code (:download:`Download <../code/two_dimensional_grid_movement.py>`):

.. literalinclude::  ../code/two_dimensional_grid_movement.py