Synchronous Axes Movement (Lockstep)
====================================

This example shows how to perform synchronous movement (lockstep) of two axes. Examples assumes one device with two axes.
Typical usage of lockstep is parallel gantry mechanism. Further technical information can be found in the following article:

https://www.zaber.com/technical-articles/driving-parallel-axes-with-lockstep-movement

Goal is to perform movement to initial position of 50 mm and then move for 3 second at slow speed.
The second axis must have constant offset of 20 mm to the first axis all the time. See the picture.

.. image:: synchronous-axes-movement.png
    :width: 500px
    :align: center
    :alt: Synchronous Axes Movement

Program will initially retrieve lockstep information from the device.
If the lockstep is not enabled program performs procedure to activate it.
Procedure first homes both axes and then set axis 2 to designated offset of 20 mm.
Then lockstep is enabled upon axis 1 and 2. From that point on both axes will move synchronously.

.. literalinclude::  ../code/synchronous_axes_movement.py
    :lines: 34-47
    :dedent: 4

Procedure of enabling lockstep serves solely to demonstrate functionality of library.
Proper lockstep setup involves mechanical mounting. Instruction from article referenced
at top of the page should be followed. In your actual automation program if lockstep is not enabled
program should most likely exit with error.

Program continues with movement execution using ``lockstep`` object.
First homing is performed following by movement to initial position of 50 mm.
Then ``lockstep.move_vel(velocity)`` starts movement at specific speed.
Program waits 3 second and stops movement using ``lockstep.stop()``.
At the end homing is performed to return device to default position.

.. literalinclude::  ../code/synchronous_axes_movement.py
    :lines: 52-68
    :dedent: 4

Once the lockstep is enabled device will always move axes synchronously.
All commands performing movement of just one axis will fail.
This includes also commands sent from other software as Zaber Console.
In order to move both axes ``lockstep`` object must be used.

Lockstep can be turned off by calling ``lockstep.disable()``.
Disabling lockstep makes axes move independently again.
Doing so when axes are mechanically tied may result in damaging devices and other equipment.

.. literalinclude::  ../code/synchronous_axes_movement.py
    :lines: 70-71
    :dedent: 4

Please change the port in the code to appropriate port on you computer (e.g. ``COM3``).
Note that when the program is running, no other program can access the computer port.

.. literalinclude::  ../code/synchronous_axes_movement.py
    :lines: 9-10

Example is using mathematical conversion between device specific microsteps and millimeters.
Correct conversion results depend on right value of constant:

.. literalinclude::  ../code/synchronous_axes_movement.py
    :lines: 4-5

Please set appropriate values depending on what device is used.
Values can be found in devices' datasheets as "Microstep Size (Default Resolution)".

More details on lockstep can be found in the following document:
https://www.zaber.com/wiki/Manuals/ASCII_Protocol_Manual#lockstep

Example code (:download:`Download <../code/synchronous_axes_movement.py>`):

.. literalinclude::  ../code/synchronous_axes_movement.py