Recording Position
==================

This example demonstrates how can you record position of the device as it is being moved manually by the knob.
Communication to the device is using ASCII protocol.

The position is recorded into CSV file along with timestamp.
CSV file named "position_log.csv" is created in the same directory the script is executed from.
If the program is executed repeatedly, file will be overwritten.
CSV file can be later opened by e.g. Microsoft Excel.

Position is recorded in microsteps. Microstep size is value specific to the device.
In order to convert position to e.g. millimeters appropriate constant "Microstep Size (Default Resolution)" from device datasheet must be used.

Please change the port in the code to appropriate port on you computer (e.g. ``COM3``).
Note that when the program is running, no other program can access the computer port.

.. literalinclude::  ../code/two_dimensional_grid_movement.py
    :lines: 4-5

Example code (:download:`Download <../code/record_position.py>`):

.. literalinclude::  ../code/record_position.py