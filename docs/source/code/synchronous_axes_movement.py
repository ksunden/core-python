import time
from zaber.serial import AsciiSerial, AsciiDevice

# device specific value used to convert distance to microsteps
DEVICE_MICROSTEP_SIZE_MICROMETERS = 0.15625  # micrometers
# zaber specific value used to convert time
DEVICE_TIME = 1.6384

# choose appropriate port on your computer (currently COM5)
SERIAL_PORT = "COM5"

# assuming the device has number 1
DEVICE_NUMBER = 1

# lockstep will be activated upon axes 1 and 2
AXIS_1_NUMBER = 1
AXIS_2_NUMBER = 2


def main():
    # choose appropriate port on your computer (currently COM5)
    port = AsciiSerial(SERIAL_PORT)

    # assuming designated device has number 1
    device = AsciiDevice(port, DEVICE_NUMBER)

    # creating manipulation objects for axes
    axis1 = device.axis(AXIS_1_NUMBER)
    axis2 = device.axis(AXIS_2_NUMBER)

    # creating manipulation object for lockstep
    lockstep = device.lockstep()

    # retrieve lockstep info
    info = lockstep.info()
    # if lockstep is not enabled execute following procedure before movement
    if not info.is_enabled:
        # home both axes
        axis1.home()
        axis2.home()

        # set axis2 to offset of 20 mm
        offset_position = int(20 * 1000 / DEVICE_MICROSTEP_SIZE_MICROMETERS)
        axis2.move_abs(offset_position)

        # enable lockstep
        lockstep.enable(axis1=AXIS_1_NUMBER, axis2=AXIS_2_NUMBER)

    # at this point lockstep is always enabled and axes move synchronously
    # from this point all movements must be performed using "lockstep" object

    # home lockstep
    lockstep.home()

    # move to initial distance of 50 mm
    initial_position = int(50 * 1000 / DEVICE_MICROSTEP_SIZE_MICROMETERS)
    lockstep.move_abs(initial_position)

    # start moving at velocity of 5 mm/s
    velocity = int(5 * 1000 / DEVICE_MICROSTEP_SIZE_MICROMETERS * DEVICE_TIME)
    lockstep.move_vel(velocity)
    # wait 3 seconds
    time.sleep(3)
    # stop movement
    lockstep.stop()

    # home lockstep
    lockstep.home()

    # uncomment following line to disable the lockstep at the end
    # lockstep.disable()


# run main function on program start
if __name__ == '__main__':
    main()
