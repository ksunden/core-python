import time
from zaber.serial import AsciiSerial, AsciiDevice

# choose appropriate port on your computer (currently COM5)
SERIAL_PORT = "COM5"

# assuming the X axis device has number 1 and Y axis device has number 2
DEVICE_X_NUMBER = 1
DEVICE_Y_NUMBER = 2

# device specific values used to convert distance to microsteps
DEVICE_X_MICROSTEP_SIZE_MICROMETERS = 0.124023437  # micrometers
DEVICE_Y_MICROSTEP_SIZE_MICROMETERS = 0.1905  # micrometers


def main():
    # create and open serial port
    port = AsciiSerial(SERIAL_PORT)

    # instantiate device for x-axis and y-axis
    device_x = AsciiDevice(port, DEVICE_X_NUMBER)
    device_y = AsciiDevice(port, DEVICE_Y_NUMBER)

    # home devices at the beginning
    device_x.home()
    device_y.home()

    # X axis - start at 2mm, move till 40mm (non-inclusive) with 5mm step
    for x_mm in range(2, 40, 5):
        # convert position on X axis to device microsteps (integer number)
        x_microsteps = int(x_mm * 1000 / DEVICE_X_MICROSTEP_SIZE_MICROMETERS)
        # order device to move to that position
        device_x.move_abs(x_microsteps)

        # Y axis - start at 2mm, move till 20mm (non-inclusive) with 4mm step
        for y_mm in range(2, 20, 4):
            # convert position on Y axis to device microsteps (integer number)
            y_microsteps = int(y_mm * 1000 / DEVICE_Y_MICROSTEP_SIZE_MICROMETERS)
            # order device to move to that position
            device_y.move_abs(y_microsteps)

            # print current position
            print("Position: x={}mm y={}mm".format(x_mm, y_mm))
            # wait 1 second before next move
            time.sleep(1)

    # home devices at the end
    device_x.home()
    device_y.home()


# run main function on program start
if __name__ == '__main__':
    main()
