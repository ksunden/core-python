.. _binary-module:

binary module
=============

BinaryCommand
-------------

.. autoclass:: zaber.serial.BinaryCommand
    :members:
    :undoc-members:
    :show-inheritance:

BinaryDevice
------------

.. autoclass:: zaber.serial.BinaryDevice
    :members:
    :undoc-members:
    :show-inheritance:

BinaryReply
-----------

.. autoclass:: zaber.serial.BinaryReply
    :members:
    :undoc-members:
    :show-inheritance:

BinarySerial
------------

.. autoclass:: zaber.serial.BinarySerial
    :members:
    :undoc-members:
    :show-inheritance:

